from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.forms import TextInput, EmailInput, DateInput, Select, RadioSelect

from userextend.models import UserExtend


class AuthenticationLoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter your username'})
        self.fields['password'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter your password'})


class UserExtendForm(UserCreationForm):
    class Meta:
        model = UserExtend
        fields = ['first_name', 'last_name', 'gender', 'date_of_birth', 'country', 'phone_number', 'email',
                  ]

        widgets = {
            'first_name': TextInput(attrs={'placeholder': 'Please enter your firstname', 'class': 'form-control'}),
            'last_name': TextInput(attrs={'placeholder': 'Please enter your lastname', 'class': 'form-control'}),
            'gender': Select(attrs={'class': 'form-select'}),
            'date_of_birth': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'country': TextInput(attrs={'placeholder': 'Please insert your country', 'class': 'form-control'}),
            'phone_number': TextInput(attrs={'placeholder': 'Please enter your phone', 'class': 'form-control'}),
            'email': EmailInput(attrs={'placeholder': 'Please enter your email', 'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs.update(
            {'class': 'form-control', 'placeholder': 'Please enter a password'})
        self.fields['password2'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Confirm your password'})
