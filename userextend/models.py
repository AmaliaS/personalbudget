from django.contrib.auth.models import AbstractUser
from django.db import models


class UserExtend(AbstractUser):
    gender_choices = (('male', 'Male'), ('female', 'Female'), ('other', 'Other'))

    gender = models.CharField(max_length=6, choices=gender_choices, null=True, blank=True)
    date_of_birth = models.DateField(null=True, blank=True)
    country = models.CharField(max_length=15, null=True, blank=True)
    phone_number = models.CharField(max_length=10, null=True, blank=True)
    newsletter = models.BooleanField(default=False, null=True, blank=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
