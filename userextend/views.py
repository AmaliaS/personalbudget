import random

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.mail import send_mail, EmailMessage
from django.shortcuts import render, redirect
from django.template.loader import get_template

from django.urls import reverse_lazy
from django.views.generic import CreateView

from PersonalBudget.settings import EMAIL_HOST_USER
from userextend.forms import UserExtendForm
from userextend.models import UserExtend


class UserCreateView(CreateView):
    template_name = 'userextend/create_user.html'
    model = UserExtend
    form_class = UserExtendForm
    success_url = reverse_lazy('list_of_incomes')


    def form_valid(self, form):
        if form.is_valid() and not form.errors:
            new_user = form.save(commit=False)
            new_user.first_name = new_user.first_name.title()
            new_user.username = f'{new_user.email}'
            new_user.save()

            # Sending a personalized email
            # subject = f'Your account has been created!'
            # message = f'Congrats, {new_user.first_name} {new_user.last_name}. \n Your usename is: {new_user.username}'
            # from_email = EMAIL_HOST_USER
            # # to_email = [f'{new_user.email}']
            # to_email = ['amalia_v92@yahoo.com']
            # send_mail(subject, message, from_email, to_email)

            # # Sending a personalized template message
            # details = {
            #     'full_name': f'{new_user.first_name} {new_user.last_name}',
            #     'username': new_user.username
            # }
            # subject = f'Contul tau a fost creat cu succes!'
            # message = get_template('email/template1.html').render(details)
            # to_email = [f'{new_user.email}']
            # # to_email = [f'{new_user.email}', 'amalia_v92@yahoo.com'] - only 1 email is sent to 2 receivers
            # msg = EmailMessage(subject, message, EMAIL_HOST_USER, to_email)
            # msg.content_subtype = "html"
            # msg.send()

        return redirect('login')
