from django import forms
from django.forms import TextInput, DateInput

from Saving.models import Saving


class SavingForm(forms.ModelForm):
    class Meta:
        model = Saving
        fields = ['category', 'amount', 'date']

        widgets = {
            'category': TextInput(attrs={'placeholder': 'Introduceti numele categoriei', 'class': 'form-control'}),
            'amount': TextInput(attrs={'placeholder': 'Valoarea tranzactiei', 'class': 'form-control'}),
            'date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),

        }
