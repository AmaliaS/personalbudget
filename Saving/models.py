from django.db import models


class Saving(models.Model):
    category = models.CharField(max_length=25)
    amount = models.IntegerField()
    date = models.DateField()

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.category}/{self.date}/{self.amount}'

    class Meta:
        ordering = ('date',)
