from django.db.models import Sum, F
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import CreateView, ListView, UpdateView

import plotly.express as px

from Expense.models import Expense
from Income.models import Income
from Saving.filters import SavingFilter
from Saving.forms import SavingForm
from Saving.models import Saving


class SavingCreateView(CreateView):
    template_name = 'saving/create_saving.html'
    model = Saving
    form_class = SavingForm
    success_url = reverse_lazy('list_of_savings')


class SavingListView(ListView):
    template_name = 'saving/list_of_savings.html'
    model = Saving
    context_object_name = 'all_savings'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()

        all_savings = Saving.objects.all()
        myFilter = SavingFilter(self.request.GET, queryset=all_savings)
        savings = myFilter.qs
        context['total_savings'] = myFilter.qs.aggregate(total=Sum(F('amount')))
        context['all_savings'] = savings
        context['my_filter'] = myFilter

        return context


class SavingUpdateView(UpdateView):
    template_name = 'saving/update_saving.html'
    model = Saving
    form_class = SavingForm
    success_url = reverse_lazy('list_of_savings')


def delete_saving_modal(request, pk):
    Saving.objects.filter(id=pk).delete()
    return redirect('list_of_savings')


def saving_dashboard(request):
    context = {}

    choice = request.GET.get('chart_choice', 'Bar')
    chart_choice = ['Bar', 'Line', 'Pie']
    context['chart_choice'] = chart_choice
    context['choice'] = choice

    all_incomes = Saving.objects.all()
    myFilter = SavingFilter(request.GET, queryset=all_incomes)
    savings = myFilter.qs
    context['all_savings'] = savings
    context['my_filter'] = myFilter

    if choice == 'Bar':
        fig = px.bar(
            x=[saving.date.strftime('%b') for saving in savings],
            y=[saving.amount for saving in savings],
            title='Savings Chart',
            labels={'x': 'Date', 'y': 'Amount'}
        )
    elif choice == 'Line':
        fig = px.line(
            x=[saving.date.strftime('%b') for saving in savings],
            y=[saving.amount for saving in savings],
            title='Savings Chart',
            labels={'x': 'Date', 'y': 'Amount'}
        )
    else:
        fig = px.pie(
            names=[saving.date.strftime('%b') for saving in savings],
            values=[saving.amount for saving in savings],
            title='Savings Chart',
            labels={'x': 'Date', 'y': 'Amount'}
        )
    fig.update_layout(
        title={
            'font_size': 22,
            'xanchor': 'center',
            'x': 0.5
        }
    )
    saving_chart = fig.to_html()
    context['saving_chart'] = saving_chart
    return render(request, 'saving/saving_dashboard.html', context)


def sav_category_dashboard(request):
    context = {}

    choice = request.GET.get('chart_choice', 'Bar')
    chart_choice = ['Bar', 'Pie']
    context['chart_choice'] = chart_choice
    context['choice'] = choice

    selected_year = int(request.GET.get('year_selected', timezone.now().year))
    all_savings = list(Saving.objects.all())
    dates = [i.date for i in all_savings]
    years = [d.year for d in dates]
    unique_years = list(set(years))
    savings = list(Saving.objects.filter(date__year=selected_year))
    context['all_savings'] = savings
    context['unique_years'] = unique_years
    context['selected_year'] = selected_year

    x_choice = request.GET.get('filter_xchoices', 'Category')
    filter_xchoices = ['Category', ]
    context['x_choice'] = x_choice
    context['filter_xchoices'] = filter_xchoices

    if choice == 'Bar' and x_choice == 'Category':
        fig = px.bar(
            x=[saving.category for saving in savings],
            y=[saving.amount for saving in savings],
            title='Savings Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Saving'}
        )

    else:
        fig = px.pie(
            names=[saving.category for saving in savings],
            values=[saving.amount for saving in savings],
            title='Savings Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Saving'}
        )

    fig.update_layout(
        title={
            'font_size': 22,
            'xanchor': 'center',
            'x': 0.5
        }
    )
    sav_category_chart = fig.to_html()
    context['sav_category_chart'] = sav_category_chart
    return render(request, 'saving/category_dashboard.html', context)


def dashboard(request):
    context = {}

    selected_date = request.GET.get('date_selected', str(timezone.now().year) + ' ' + str(timezone.now().month))
    selected_year, selected_month = selected_date.split(' ')

    all_incomes = list(Income.objects.all())
    idates = [i.transaction_date for i in all_incomes]
    iym = [str(d.year) + ' ' + str(d.month) for d in idates]
    all_expenses = list(Expense.objects.all())
    edates = [i.transaction_date for i in all_expenses]
    eym = [str(d.year) + ' ' + str(d.month) for d in edates]
    unique_dates = list(set(iym + eym))
    unique_dates.sort(reverse=True)

    incomes_amount = \
        Income.objects.filter(transaction_date__year=selected_year).aggregate(total=Sum(F('monthly_rate')))['total']
    expenses_amount = Expense.objects.filter(transaction_date__year=selected_year).aggregate(total=Sum(F('amount')))[
        'total']

    incomes_amount_month = \
        Income.objects.filter(transaction_date__month=selected_month).aggregate(total=Sum(F('monthly_rate')))['total']
    expenses_amount_month = \
        Expense.objects.filter(transaction_date__month=selected_month).aggregate(total=Sum(F('amount')))['total']

    context['all_incomes'] = incomes_amount
    context['all_expenses'] = expenses_amount

    context['all_incomes_month'] = incomes_amount_month
    context['all_expenses_month'] = expenses_amount_month

    context['unique_dates'] = unique_dates
    context['selected_date'] = selected_date
    context['selected_year'] = selected_year
    context['selected_month'] = selected_month

    # balance - year
    if incomes_amount is not None and expenses_amount is not None:
        balance = incomes_amount - expenses_amount
        context['balance'] = balance
        if incomes_amount_month is not None and expenses_amount_month is not None:
            balance = incomes_amount - expenses_amount
            balance_month = incomes_amount_month - expenses_amount_month
            daily_rate = incomes_amount_month // 21
            working_days = expenses_amount_month / daily_rate
            balance_days = 21 - working_days
            context['balance_month'] = balance_month
            context['daily_rate'] = daily_rate
            context['working_days'] = working_days
            context['balance_days'] = balance_days
            context['balance'] = balance

        elif incomes_amount_month is not None and expenses_amount_month is None:
            balance_month = incomes_amount_month
            daily_rate = incomes_amount_month // 21
            working_days = 0
            balance_days = 21
            context['balance_month'] = balance_month
            context['daily_rate'] = daily_rate
            context['working_days'] = working_days
            context['balance_days'] = balance_days

        elif incomes_amount_month is None and expenses_amount_month is not None:
            balance_month = -expenses_amount_month
            daily_rate = 0
            working_days = 0
            balance_days = 0
            context['balance_month'] = balance_month
            context['daily_rate'] = daily_rate
            context['working_days'] = working_days
            context['balance_days'] = balance_days

        else:
            balance_month = 0
            daily_rate = 0
            working_days = 0
            balance_days = 0
            context['balance_month'] = balance_month
            context['daily_rate'] = daily_rate
            context['working_days'] = working_days
            context['balance_days'] = balance_days

    elif incomes_amount is not None and expenses_amount is None:
        balance = incomes_amount
        context['balance'] = balance
        balance_month = incomes_amount_month
        daily_rate = incomes_amount_month // 21
        working_days = 0
        balance_days = 21
        context['balance_month'] = balance_month
        context['daily_rate'] = daily_rate
        context['working_days'] = working_days
        context['balance_days'] = balance_days

    elif incomes_amount is None and expenses_amount is not None:
        balance = -expenses_amount
        context['balance'] = balance
        balance_month = -expenses_amount_month
        daily_rate = 0
        working_days = 0
        balance_days = 0
        context['balance_month'] = balance_month
        context['daily_rate'] = daily_rate
        context['working_days'] = working_days
        context['balance_days'] = balance_days
    else:
        balance = 0
        context['balance'] = balance
        balance_month = 0
        daily_rate = 0
        working_days = 0
        balance_days = 0
        context['balance_month'] = balance_month
        context['daily_rate'] = daily_rate
        context['working_days'] = working_days
        context['balance_days'] = balance_days

    context['zero'] = 0

    return render(request, 'saving/Dashboard.html', context)

