from django.urls import path

from Saving import views

urlpatterns = [
    path('create-saving/', views.SavingCreateView.as_view(), name='create_saving'),
    path('list-of-savings/', views.SavingListView.as_view(), name='list_of_savings'),
    path('update-saving/<int:pk>', views.SavingUpdateView.as_view(), name='update_saving'),
    path('delete-saving-modal/<int:pk>', views.delete_saving_modal, name='delete_saving_modal'),
    path('saving-dashboard/', views.saving_dashboard, name='saving_dashboard'),
    path('sav-category-dashboard/', views.sav_category_dashboard, name='sav_category_dashboard'),
    path('dashboard/', views.dashboard, name='dashboard'),

]
