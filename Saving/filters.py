import django_filters
from django_filters import DateFilter

from Saving import forms
from Saving.models import Saving


class SavingFilter(django_filters.FilterSet):
    category = django_filters.CharFilter(lookup_expr='icontains', label="Category")
    amount = django_filters.NumberFilter()
    amount_gte = django_filters.NumberFilter(field_name='amount', lookup_expr='gte')
    amount_lte = django_filters.NumberFilter(field_name='amount', lookup_expr='lte')

    date_gte = DateFilter(field_name='date', lookup_expr='gte',
                          widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))
    date_lte = DateFilter(field_name='date', lookup_expr='lte',
                          widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))

    class Meta:
        model = Saving
        fields = ['category', 'amount', 'amount_gte', 'amount_lte', 'date_gte', 'date_lte', ]

    def __init__(self, *args, **kwargs):
        super(SavingFilter, self).__init__(*args, **kwargs)

        self.filters['category'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Categorie'})
        self.filters['date_gte'].field.widget.attrs.update({'class': 'form-control form-control-sm'})
        self.filters['date_lte'].field.widget.attrs.update({'class': 'form-control form-control-sm'})
        self.filters['amount'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Amount'})
        self.filters['amount_gte'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Greater than'})
        self.filters['amount_lte'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Lower than'})
