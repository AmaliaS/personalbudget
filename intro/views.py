from django.http import HttpResponse
from django.shortcuts import render


def intro_page(request):
    # return HttpResponse('<h1>Welcome!</h1>')
    return render(request, 'intro/intro_page.html')


def help_page(request):
    # return HttpResponse('help_page.html')
    return render(request, 'intro/help_page.html')
