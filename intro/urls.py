
from django.urls import path

from intro import views

urlpatterns = [
    path('', views.intro_page, name='intro_page'),
    path('help-page/', views.help_page, name='help_page'),
]