from django import forms
from django.forms import Select, DateInput, TextInput

from Income.models import Income


class IncomeForm(forms.ModelForm):
    class Meta:
        model = Income
        fields = ['category', 'subcategory', 'type', 'transaction_date', 'monthly_rate']

        widgets = {
            'category': TextInput(attrs={'placeholder': 'Please enter the category name', 'class': 'form-control'}),
            'subcategory': TextInput(
                attrs={'placeholder': 'Please enter the subcategory name', 'class': 'form-control'}),
            'type': Select(attrs={'class': 'form-select'}),
            'transaction_date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'monthly_rate': TextInput(attrs={'placeholder': 'Please enter the monthly rate', 'class': 'form-control'})

        }
