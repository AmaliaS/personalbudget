from django.urls import path

from Income import views

urlpatterns = [
    path('create-income/', views.IncomeCreateView.as_view(), name='create_income'),
    path('list-of-incomes/', views.IncomeListView.as_view(), name='list_of_incomes'),
    path('update-income/<int:pk>', views.IncomeUpdateView.as_view(), name='update_income'),
    path('delete-income-modal/<int:pk>', views.delete_income_modal, name='delete_income_modal'),
    path('income-dashboard/', views.income_dashboard, name='income_dashboard'),
    path('in-category-dashboard/', views.in_category_dashboard, name='in_category_dashboard'),

]
