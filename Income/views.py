from django.db.models import Sum, F
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import CreateView, ListView, UpdateView

import plotly.express as px

from Income.filters import IncomeFilter
from Income.forms import IncomeForm
from Income.models import Income


class IncomeCreateView(CreateView):
    template_name = 'income/create_income.html'
    model = Income
    form_class = IncomeForm
    success_url = reverse_lazy('list_of_incomes')

    def form_valid(self, form):
        if form.is_valid and not form.errors:
            new_income = form.save(commit=False)
            new_income.daily_rate = new_income.monthly_rate // 21
            new_income.annual_rate = new_income.monthly_rate * 12
            new_income.save()
        return redirect('list_of_incomes')


class IncomeListView(ListView):
    template_name = 'income/list_of_incomes.html'
    model = Income
    context_object_name = 'all_incomes'

    def get_context_data(self, *, object_list=None, **kwargs):

        context = super().get_context_data()

        # filter + total
        all_incomes = Income.objects.all()
        myFilter = IncomeFilter(self.request.GET, queryset=all_incomes)
        incomes = myFilter.qs
        context['total_income_month'] = myFilter.qs.aggregate(total=Sum(F('monthly_rate')))
        context['all_incomes'] = incomes
        context['my_filter'] = myFilter

        return context


class IncomeUpdateView(UpdateView):
    template_name = 'income/update_income.html'
    model = Income
    form_class = IncomeForm
    success_url = reverse_lazy('list_of_incomes')


def delete_income_modal(request, pk):
    Income.objects.filter(id=pk).delete()
    return redirect('list_of_incomes')


# --------------------------------------------------------------------------------
def income_dashboard(request):
    context = {}

    # chart filter
    choice = request.GET.get('chart_choice', 'Bar')
    chart_choice = ['Bar', 'Line', 'Pie']
    context['chart_choice'] = chart_choice
    context['choice'] = choice

    # data filter
    all_incomes = Income.objects.all()
    if all_incomes is not None:
        myFilter = IncomeFilter(request.GET, queryset=all_incomes)
        incomes = myFilter.qs
        context['all_incomes'] = incomes
        context['my_filter'] = myFilter

        if choice == 'Bar':
            fig = px.bar(
                x=[income.transaction_date.strftime('%b') for income in incomes],
                y=[income.monthly_rate for income in incomes],
                title='Incomes Chart',
                labels={'x': 'Date', 'y': 'Monthly Income'},
            )
        elif choice == 'Line':
            fig = px.line(
                x=[income.transaction_date.strftime('%b') for income in incomes],
                y=[income.monthly_rate for income in incomes],
                title='Incomes Chart',
                labels={'x': 'Date', 'y': 'Monthly Income'},
            )
        else:
            fig = px.pie(
                names=[income.transaction_date.strftime('%b') for income in incomes],
                values=[income.monthly_rate for income in incomes],
                title='Incomes Chart',
                labels={'x': 'Date', 'y': 'Monthly Income'}
            )
        fig.update_layout(
            title={
                'font_size': 22,
                'xanchor': 'center',
                'x': 0.5
            },
        )
        income_chart = fig.to_html()
        context['income_chart'] = income_chart
    else:
        text = 'Please insert data in your income list!'
        context['text'] = text
    return render(request, 'income/income_dashboard.html', context)


def in_category_dashboard(request):
    context = {}

    # chart filter
    choice = request.GET.get('chart_choice', 'Bar')
    chart_choice = ['Bar', 'Pie']
    context['chart_choice'] = chart_choice
    context['choice'] = choice

    # time filter
    selected_year = int(request.GET.get('year_selected', timezone.now().year))
    all_incomes = list(Income.objects.all())
    dates = [i.transaction_date for i in all_incomes]
    years = [d.year for d in dates]
    unique_years = list(set(years))
    incomes = list(Income.objects.filter(transaction_date__year=selected_year))
    context['all_incomes'] = incomes
    context['unique_years'] = unique_years
    context['selected_year'] = selected_year

    # x filter
    x_choice = request.GET.get('filter_xchoices', 'Category')
    filter_xchoices = ['Category', 'Subcategory', 'Type']
    context['x_choice'] = x_choice
    context['filter_xchoices'] = filter_xchoices

    if choice == 'Bar' and x_choice == 'Category':
        fig = px.bar(
            x=[income.category for income in incomes],
            y=[income.monthly_rate for income in incomes],
            title='Incomes Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Income'}
        )

    elif choice == 'Pie' and x_choice == 'Category':
        fig = px.pie(
            names=[income.category for income in incomes],
            values=[income.monthly_rate for income in incomes],
            title='Incomes Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Income'}
        )

    elif choice == 'Bar' and x_choice == 'Subcategory':
        fig = px.bar(
            x=[income.subcategory for income in incomes],
            y=[income.monthly_rate for income in incomes],
            title='Incomes Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Income'}
        )

    elif choice == 'Pie' and x_choice == 'Subcategory':
        fig = px.pie(
            names=[income.subcategory for income in incomes],
            values=[income.monthly_rate for income in incomes],
            title='Incomes Comparison Chart',
            labels={'x': 'Subcategory', 'y': 'Annual Income'}
        )
    elif choice == 'Bar' and x_choice == 'Type':
        fig = px.bar(
            x=[income.type for income in incomes],
            y=[income.monthly_rate for income in incomes],
            title='Incomes Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Income'}
        )

    else:
        fig = px.pie(
            names=[income.type for income in incomes],
            values=[income.monthly_rate for income in incomes],
            title='Incomes Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Income'}
        )

    fig.update_layout(
        title={
            'font_size': 22,
            'xanchor': 'center',
            'x': 0.5
        }
    )
    in_category_chart = fig.to_html()
    context['in_category_chart'] = in_category_chart
    return render(request, 'income/category_dashboard.html', context)

