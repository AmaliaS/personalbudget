from django.db import models


class Income(models.Model):
    type_choices = (('Active', 'Active'), ('Passive', 'Passive'))

    category = models.CharField(max_length=20)
    subcategory = models.CharField(max_length=20)
    type = models.CharField(max_length=8, choices=type_choices)
    transaction_date = models.DateField()
    monthly_rate = models.IntegerField()

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.category}/{self.subcategory}/{self.transaction_date}/{self.monthly_rate}'

    class Meta:
        ordering = ('transaction_date',)
