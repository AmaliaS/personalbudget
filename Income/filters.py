import django_filters
from django_filters import DateFilter

from Income import forms
from Income.models import Income


class IncomeFilter(django_filters.FilterSet):
    category = django_filters.CharFilter(lookup_expr='icontains', label="Category")
    subcategory = django_filters.CharFilter(lookup_expr='icontains', label="Subcategory")
    monthly_rate = django_filters.NumberFilter()
    monthly_rate_gte = django_filters.NumberFilter(field_name='monthly_rate', lookup_expr='gte')
    monthly_rate_lte = django_filters.NumberFilter(field_name='monthly_rate', lookup_expr='lte')

    transaction_date_gte = DateFilter(field_name='transaction_date', lookup_expr='gte',
                                      widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))
    transaction_date_lte = DateFilter(field_name='transaction_date', lookup_expr='lte',
                                      widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))

    class Meta:
        model = Income
        fields = ['category', 'subcategory', 'type', 'transaction_date_gte', 'transaction_date_lte', 'monthly_rate',
                  'monthly_rate_gte', 'monthly_rate_lte']

    def __init__(self, *args, **kwargs):
        super(IncomeFilter, self).__init__(*args, **kwargs)

        self.filters['category'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Categorie'})
        self.filters['subcategory'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Subcategorie'})
        self.filters['type'].field.widget.attrs.update({'class': 'form-select form-select-sm'})
        self.filters['transaction_date_gte'].field.widget.attrs.update({'class': 'form-control form-control-sm'})
        self.filters['transaction_date_lte'].field.widget.attrs.update({'class': 'form-control form-control-sm'})
        self.filters['monthly_rate'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Amount'})
        self.filters['monthly_rate_gte'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Greater than'})
        self.filters['monthly_rate_lte'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Lower than'})
