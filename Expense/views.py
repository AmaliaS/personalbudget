from django.db.models import Sum, F
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.views.generic import CreateView, ListView, UpdateView

import plotly.express as px

from Expense.filters import ExpenseFilter
from Expense.forms import ExpenseForm
from Expense.models import Expense

class ExpenseCreateView(CreateView):
    template_name = 'expense/create_expense.html'
    model = Expense
    form_class = ExpenseForm
    success_url = reverse_lazy('list_of_expenses')

class ExpenseListView(ListView):
    template_name = 'expense/list_of_expenses.html'
    model = Expense
    context_object_name = 'all_expenses'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data()

        all_expenses = Expense.objects.all()
        myFilter = ExpenseFilter(self.request.GET, queryset=all_expenses)
        expenses = myFilter.qs
        context['total_expenses'] = myFilter.qs.aggregate(total=Sum(F('amount')))
        context['all_expenses'] = expenses
        context['my_filter'] = myFilter

        return context

class ExpenseUpdateView(UpdateView):
    template_name = 'expense/update_expense.html'
    model = Expense
    form_class = ExpenseForm
    success_url = reverse_lazy('list_of_expenses')

def delete_expense_modal(request,pk):
    Expense.objects.filter(id=pk).delete()
    return redirect('list_of_expenses')

def expense_dashboard(request):
    context = {}

    choice = request.GET.get('chart_choice', 'Bar')
    chart_choice = ['Bar', 'Line', 'Pie']
    context['chart_choice'] = chart_choice
    context['choice'] = choice

    all_expenses = Expense.objects.all()
    myFilter = ExpenseFilter(request.GET, queryset=all_expenses)
    expenses = myFilter.qs
    context['all_expenses'] = expenses
    context['my_filter'] = myFilter

    if choice == 'Bar':
        fig = px.bar(
            x=[expense.transaction_date.strftime('%b') for expense in expenses],
            y=[expense.amount for expense in expenses],
            title='Expenses Chart',
            labels={'x': 'Date', 'y': 'Amount'}
        )
    elif choice == 'Line':
        fig = px.line(
            x=[expense.transaction_date.strftime('%b') for expense in expenses],
            y=[expense.amount for expense in expenses],
            title='Expenses Chart',
            labels={'x': 'Date', 'y': 'Amount'}
        )
    else:
        fig = px.pie(
            names=[expense.transaction_date.strftime('%b') for expense in expenses],
            values=[expense.amount for expense in expenses],
            title='Expenses Chart',
            labels={'x': 'Date', 'y': 'Amount'}
        )
    fig.update_layout(
        title={
            'font_size': 22,
            'xanchor': 'center',
            'x': 0.5
        }
    )
    expense_chart = fig.to_html()
    context['expense_chart'] = expense_chart
    return render(request, 'expense/expense_dashboard.html', context)

def ex_category_dashboard(request):

    context = {}

    choice = request.GET.get('chart_choice', 'Bar')
    chart_choice = ['Bar', 'Pie']
    context['chart_choice'] = chart_choice
    context['choice'] = choice

    selected_year = int(request.GET.get('year_selected', timezone.now().year))
    all_expenses = list(Expense.objects.all())
    dates = [i.transaction_date for i in all_expenses]
    years = [d.year for d in dates]
    unique_years = list(set(years))
    expenses = list(Expense.objects.filter(transaction_date__year=selected_year))
    context['all_expenses'] = expenses
    context['unique_years'] = unique_years
    context['selected_year'] = selected_year

    x_choice = request.GET.get('filter_xchoices', 'Category')
    filter_xchoices = ['Category', 'Subcategory', 'Type']
    context['x_choice'] = x_choice
    context['filter_xchoices'] = filter_xchoices

    if choice == 'Bar' and x_choice == 'Category':
        fig = px.bar(
            x=[expense.category for expense in expenses],
            y=[expense.amount for expense in expenses],
            title='Expenses Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Expense'}
        )

    elif choice == 'Pie' and x_choice == 'Category':
        fig = px.pie(
            names=[expense.category for expense in expenses],
            values=[expense.amount for expense in expenses],
            title='Expenses Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Expense'}
        )

    elif choice == 'Bar' and x_choice == 'Subcategory':
        fig = px.bar(
            x=[expense.subcategory for expense in expenses],
            y=[expense.amount for expense in expenses],
            title='Expenses Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Expense'}
        )

    elif choice == 'Pie' and x_choice == 'Subcategory':
        fig = px.pie(
            names=[expense.subcategory for expense in expenses],
            values=[expense.amount for expense in expenses],
            title='Expenses Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Expense'}
        )
    elif choice == 'Bar' and x_choice == 'Type':
        fig = px.bar(
            x=[expense.type for expense in expenses],
            y=[expense.amount for expense in expenses],
            title='Expenses Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Expense'}
        )

    else:
        fig = px.pie(
            names=[expense.type for expense in expenses],
            values=[expense.amount for expense in expenses],
            title='Expenses Comparison Chart',
            labels={'x': x_choice, 'y': 'Annual Expense'}
        )

    fig.update_layout(
        title={
            'font_size': 22,
            'xanchor': 'center',
            'x': 0.5
        }
    )
    ex_category_chart = fig.to_html()
    context['ex_category_chart'] = ex_category_chart
    return render(request, 'expense/category_dashboard.html', context)





