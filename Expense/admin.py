from django.contrib import admin

# Register your models here.
from Expense.models import Expense

admin.site.register(Expense)
