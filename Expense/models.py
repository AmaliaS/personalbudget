from django.db import models
from django.db.models import Sum, F

from Income.models import Income


class Expense(models.Model):
    type_choices = (('Vital', 'Vital'), ('Optional', 'Optional'))
    category = models.CharField(max_length=20)
    subcategory = models.CharField(max_length=20)
    type = models.CharField(max_length=9, choices=type_choices)
    transaction_date = models.DateField()
    amount = models.IntegerField()
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.category}/{self.subcategory}/{self.transaction_date}/{self.amount}'

    def working_days(self):
        selected_expense = list(Expense.objects.filter(id=self.id))
        amount = [i.amount for i in selected_expense]
        selected_amount = amount[0]
        date = [i.transaction_date for i in selected_expense]
        year = [d.year for d in date]
        selected_year = year[0]
        month = [d.month for d in date]
        selected_month = month[0]

        incomes_amount_month = \
            Income.objects.filter(transaction_date__year=selected_year,
                                  transaction_date__month=selected_month).aggregate(total=Sum(F('monthly_rate')))[
                'total']

        if incomes_amount_month is not None:
            daily_rate = incomes_amount_month // 21
            working_days = selected_amount / daily_rate
            return working_days
        else:
            working_days = 0
            return working_days

    class Meta:
        ordering = ('transaction_date',)
