import django_filters
from django_filters import DateFilter

from Expense import forms
from Expense.models import Expense


class ExpenseFilter(django_filters.FilterSet):
    category = django_filters.CharFilter(lookup_expr='icontains', label="Category")
    subcategory = django_filters.CharFilter(lookup_expr='icontains', label="Subcategory")
    amount = django_filters.NumberFilter()
    amount_gte = django_filters.NumberFilter(field_name='amount', lookup_expr='gte')
    amount_lte = django_filters.NumberFilter(field_name='amount', lookup_expr='lte')

    transaction_date_gte = DateFilter(field_name='transaction_date', lookup_expr='gte',
                                      widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))
    transaction_date_lte = DateFilter(field_name='transaction_date', lookup_expr='lte',
                                      widget=forms.DateInput(attrs={'class': 'form-control', 'type': 'date'}))

    class Meta:
        model = Expense
        fields = ['category', 'subcategory', 'type', 'transaction_date_gte', 'transaction_date_lte', 'amount',
                  'amount_gte', 'amount_lte']

    def __init__(self, *args, **kwargs):
        super(ExpenseFilter, self).__init__(*args, **kwargs)

        self.filters['category'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Categorie'})
        self.filters['subcategory'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Subcategorie'})
        self.filters['type'].field.widget.attrs.update({'class': 'form-select form-select-sm'})
        self.filters['transaction_date_gte'].field.widget.attrs.update({'class': 'form-control form-control-sm'})
        self.filters['transaction_date_lte'].field.widget.attrs.update({'class': 'form-control form-control-sm'})
        self.filters['amount'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Amount'})
        self.filters['amount_gte'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Greater than'})
        self.filters['amount_lte'].field.widget.attrs.update(
            {'class': 'form-control form-control-sm', 'placeholder': 'Lower than'})
