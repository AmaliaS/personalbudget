from django import forms
from django.forms import Select, DateInput, TextInput

from Expense.models import Expense


class ExpenseForm(forms.ModelForm):
    class Meta:
        model = Expense
        fields = ['category', 'subcategory', 'type', 'transaction_date', 'amount']

        widgets = {
            'category': TextInput(attrs={'placeholder': 'Please enter the category name', 'class': 'form-control'}),
            'subcategory': TextInput(
                attrs={'placeholder': 'Please enter the subcategory name', 'class': 'form-control'}),
            'type': Select(attrs={'class': 'form-select'}),
            'transaction_date': DateInput(attrs={'class': 'form-control', 'type': 'date'}),
            'amount': TextInput(attrs={'placeholder': 'Please enter the monthly rate', 'class': 'form-control'})

        }
