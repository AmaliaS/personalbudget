from django.urls import path

from Expense import views

urlpatterns = [
    path('create-expense/', views.ExpenseCreateView.as_view(), name='create_expense'),
    path('list-of-expenses/', views.ExpenseListView.as_view(), name='list_of_expenses'),
    path('update-expense/<int:pk>', views.ExpenseUpdateView.as_view(), name='update_expense'),
    path('delete-expense-modal/<int:pk>', views.delete_expense_modal, name='delete_expense_modal'),
    path('expense-dashboard/', views.expense_dashboard, name='expense_dashboard'),
    path('ex-category-dashboard/', views.ex_category_dashboard, name='ex_category_dashboard'),
]
